function worker(gen, ...args) {
  const iter = gen(...args);
  const res = iter.next();

  function innerHandler(res) {
    if (typeof res.value === 'function') {
      try {
        let resToPass = res.value();
        const curRes = iter.next(resToPass);

        return innerHandler(curRes);
      } catch (error) {
        return innerHandler(iter.throw());
      }
    }

    if (res.value instanceof Promise) {
      return res.value.then(res => innerHandler(iter.next(res)));
    }

    if (res.done) {
      return res.value;
    }

    return innerHandler(iter.next(res.value));
  };

return new Promise((resolve) => resolve(innerHandler(res)));
};
